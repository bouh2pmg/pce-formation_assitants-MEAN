﻿Quick tutorial

**MEAN(2)**


![image alt text](image_0.png)

![image alt text](image_1.png)


## Introduction

 The purpose of this quick tutorial is to give you an overview of what MEAN is and how to use it. We assume that you have the basic object programming knowledge so everything will not be explained. 

  MEAN refers to a collection of JavaScript based technologies used to develop web applications. MEAN is an acronym for MongoDB, ExpressJS, Angular and Node.js. From client to server to database, MEAN is full stack JavaScript 

Node.js is a server side JavaScript execution environment. It helps in building highly scalable and concurrent applications rapidly.

Express is lightweight framework used to build web applications in Node. It provides a number of robust features for building single and multi page web application. 

MongoDB is a schemaless NoSQL database system. MongoDB saves data in binary JSON format which makes it easier to pass data between client and server.

Angular is a JavaScript framework developed by Google. It provides some awesome features like the two-way data binding. It’s a complete solution for rapid and awesome front end development.

  After reading this tutorial you will have the basic knowledge about the MEAN stack, you will be able to :

* Display data on the Angular side from MongoDB 

* Push data from the Angular side to MongoDB

* Interact with the html (use form)

* Render the html from the Express server

You can clone the source of this tutorial at this address : git@gitlab.com:webac/formation/MEAN-StarterPack.git

## Instructions

First make sure you have installed and configured all of theses tools : 

* Download/install/configure NodeJS following this link (NPM is uncluded) :  https://nodejs.org/en/

* Configure Angular : 

We're going to use the Angular CLI to create our Angular app. Let's install it at the command line through NPM.

  

    $ npm install @angular/cli -g

  

Once installed, go into the folder where you prefer to store your projects and run the following command:

  

    $ ng new mean
    $ cd mean

  

Once you're in the new project folder, we're going to run a command with the Angular CLI that will create a build of our project. We need to do this because our** Express server is going to look for a /dist** folder to serve the files.

  

    $ ng build

  

* Configure Express.js: 

While still inside of the project folder, we're going to use NPM to install a few different packages. Run the following command:

  

    $ npm install express body-parser --save

  

This will install the** Express** server package and body-parser. In order to handle** HTTP POST** request in** Express.js version 4 and above, you need to install a middleware** module called body-parser. body-parser extract the entire body portion of an incoming request stream and exposes it on req.body.

--save is used here so it will update your package.json and set this 2 package as project dependencies.

Using your code editor, open up the project folder and create a new file** server.js** in the base project folder (mean)

In /server.js paste the following contents:

  

   

    const express = require('express');
    
    const bodyParser = require('body-parser');
    
    const path = require('path');
    
    const http = require('http');
    
    const app = express();
    
    // API file for interacting with MongoDB
    
    const api = require('./server/routes/api');
    
    // Parsers
    
    app.use(bodyParser.json());
    
    app.use(bodyParser.urlencoded({ extended: false}));
    
    // Angular DIST output folder
    
    app.use(express.static(path.join(__dirname, 'dist')));
    
    // API location
    
    app.use('/api', api);
    
    // Send all other requests to the Angular app
    
    app.get('*', (req, res) => {
    
        res.sendFile(path.join(__dirname, 'dist/index.html'));
    
    });
    
    //Set Port
    
    const port = process.env.PORT || '3000';
    
    app.set('port', port);
    
    const server = http.createServer(app);
    
    server.listen(port, () => console.log(Running on localhost:${port}));

  

The basic comments describe what's happening above. The above configuration will work for most apps and you shouldn't need to modify it.

* Download/install/configure MongoDb following this link :  https://www.mongodb.com 

For instance, on Windows, you have to start MongoDB server at the command prompt:

  

       "C:\Program Files\MongoDB\Server\3.4\bin\mongod.exe"

  

Then, you have to open another command prompt and connect to it through the shell by running:

  

 

       "C:\Program Files\MongoDB\Server\3.4\bin\mongo.exe"

  

Once you're in the shell, run the following commands to create a database:

  

 

    > use mean
    > db.users.insert({"name":"John Doe"})

  

Here, we're creating the database and inserting at least 1 document into it.

Back in the console within the project folder, run the following command:

  

    $ npm install mongodb --save

  

mongodb is a package that allows us to interact with MongoDB.

Back in the code editor, create the following project structure:

  

    /mean (project folder)
	   /server
	      /routes

  

You're creating 2 folders, one as server and the other inside server, which is routes.

Inside of the routes folder, create a new file called api.js. Inside of it, paste the following:

    const express = require('express');
    
    const router = express.Router();
    
    const MongoClient = require('mongodb').MongoClient;
    
    const ObjectID = require('mongodb').ObjectID;
    
    // Connect
    
    const connection = (closure) => {
    
        return MongoClient.connect('mongodb://localhost:27017/mean', (err, client) => {
    
            if (err) return console.log(err);
    
            closure(client);
    
        });
    
    };
    
    // Error handling
    
    const sendError = (err, res) => {
    
        response.status = 501;
    
        response.message = typeof err == 'object' ? err.message : err;
    
        res.status(501).json(response);
    
    };
    
    // Response handling
    
    let response = {
    
        status: 200,
    
        data: [],
    
        message: null
    
    };
    
    // Get users
    
    router.get('/users', (req, res) => {
    
        connection((client) => {
    
            var db = client.db('mean');
    
            db.collection('users')
    
                .find()
    
                .toArray()
    
                .then((users) => {
    
                    response.data = users;
    
                    res.json(response);
    
                })
    
                .catch((err) => {
    
                    sendError(err, res);
    
                    client.close();
    
                });
    
        });
    
    });
    
    module.exports = router;

  

Save this. Back in the console, run the following command in the project folder:

  

    $ node server

  

Then, visit** http://localhost:3000/api/users** and with any luck, the output should be:

  

    {"status":200,"users":[{"_id":"598c9da2f7d6d70def3d9f6c","name":"John Doe"}

  

You can try adding more documents within the collection and refreshing this URL.

* Fetch data from the Angular App

The last order of business is to display the data that we've retrieved from MongoDB. Right now, we're only accessing it by directly visiting the API endpoint URL, which is useless.

We're going to use the Angular CLI to create a service file for communicating with the API.

    $ ng g service data

Open up the new file that was generated /src/app/data.service.ts and paste the following:

    import { Injectable } from '@angular/core';
    
    import { Http, Headers, RequestOptions } from '@angular/http';
    
    import 'rxjs/add/operator/map';
    
    @Injectable()
    
    export class DataService {
    
      result:any;
    
      constructor(private _http: Http) { }
    
      getUsers() {
    
        return this._http.get("/api/users")
    
          .map(result => this.result = result.json().data);
    
      }
    
    }

  

We're importing necessary functions from the http library along with the map operator to handle response data from the API.

We're also declaring a getUsers() method that communicates with the API endpoint we defined for getting all users** /api/users. We use the map operator to take the response, which contains our object, and we're grabbing just the .data** in json() format.

Before we can use this service file, it has to be provided in our /src/app/app.module.ts file:

  

    import { BrowserModule } from '@angular/platform-browser';
    
    import { NgModule } from '@angular/core';
    
    import { AppComponent } from './app.component';
    
    // Import the Http Module and our Data Service
    
    import { HttpModule } from '@angular/http';
    
    import { DataService } from './data.service';
    
    @NgModule({
    
      declarations: [
    
        AppComponent
    
      ],
    
      imports: [
    
        BrowserModule,
    
        HttpModule              // <-Add HttpModule
    
      ],
    
      providers: [DataService], // <-Add DataService
    
      bootstrap: [AppComponent]
    
    })
    
    export class AppModule { }

  

Now, let's say we want to** show all of the users** when the app loads. Well, open up /src/app/app.component.ts file and add the following:

  

  

     import { Component } from '@angular/core';
    
    // Import the DataService
    
    import { DataService } from './data.service';
    
    @Component({
    
      selector: 'app-root',
    
      templateUrl: './app.component.html',
    
      styleUrls: ['./app.component.css']
    
    })
    
    export class AppComponent {
    
      
    
      // Define a users property to hold our user data
    
      users: Array<any>;
    
      // Create an instance of the DataService through dependency injection
    
      constructor(private _dataService: DataService) {
    
        // Access the Data Service's getUsers() method we defined
    
        this._dataService.getUsers()
    
            .subscribe(res => this.users = res);
    
      }
    
    }

  

The last order of business is to modify the** /src/app/app.component.html** template file:

  

        <h1>Our MongoDB is Working!</h1>
	    <ul>
	      <li *ngFor="let user of users">{{ user.name }}</li>
	    </ul>

  

Go back to the console and run ng build to rebuild our modified** Angular app.**

Then run node server. Visit localhost:3000 in the browser, and with any luck, it should show any of the documents you added through the mongo shell.

And that’s it ! You have a workin MEAN app !

# Going Further

In a regular CRUD app (Create Read Update Delete), this is simply Read, as we're reading from the database. 

To handling creation, updating and deleting, the steps are as follows:

\1. Modify theb  **/server/routes/api.js** to include a new endpoint for either creating, updating or deleting. The syntax is very similar, but you may need to use Google to find examples.

\2. Modify the **/src/app/data.service.ts** file. You would create a new method for communicating with the API.

\3. Use the new methods in your components.

Let’s do an exemple, we are going to create a form and use it to insert new users.

First, we are going to create the route, go to /server/routes/api.js  and add : 

  

   

    // Post a user
    
    router.post('/user', (req, res) => {
    
        connection((client) => {
    
            var db = client.db('mean');
    
            db.collection("users")
    
                .insertOne(req.body)
    
                .then((result) => {
    
                    response.data = result;
    
                    res.json(response);
    
                })
    
                .catch((err) => {
    
                    sendError(err, res);
    
                    client.close();
    
                });
    
        });
    
    });

  

Now that our route is set up, go to your /src/app/data.service.ts and add this function : 

  

    postUser(user: Object) {

    return this._http.post("/api/user", user)

      .map(result => this.result = result.json().data);

  }

  

This will perform a http POST request to the route we just create, and return the data as JSON.

Now we are going to set up our HTML and our Angular component. First, go into your** /styles.css** and add this new line : 

  

       @import url('https://unpkg.com/bootstrap@3.3.7/dist/css/bootstrap.min.css'); 
    
    /* Add this to get quick access to bootstrap CSS */

  

Then go to your** /src/app/app.module.ts** and add the FormsModule import  : 

  

    import { BrowserModule } from '@angular/platform-browser';
    
    import { NgModule } from '@angular/core';
    
    import { FormsModule }   from '@angular/forms'; // Add this to get forms working.
    
    import { AppComponent } from './app.component';
    
    import { HttpModule } from '@angular/http';
    
    import { DataService } from './data.service';
    
    @NgModule({
    
      declarations: [
    
        AppComponent
    
      ],
    
      imports: [
    
        FormsModule, // Add the module to the 'Imports' array.
    
        HttpModule,
    
        BrowserModule
    
      ],
    
      providers: [DataService],
    
      bootstrap: [AppComponent]
    
    })
    
    export class AppModule { }

  

Then go in your /src/app/app.component.html and create the html form :

  

       <div class="container">
    
      <h1>
    
        Welcome to {{ title }}!
    
      </h1>
    
      <div class="container">
    
        <form #userForm="ngForm" (ngSubmit)="postUser()">
    
          <div class="form-group">
    
            <label for="name">Nom</label>
    
            <input class="form-control" id="name" name="name" type="text" [(ngModel)]="user.name" #username="ngModel" required>
    
            <span [hidden]="username.valid" class="text-danger">Vous devez entrer un nom !</span>
    
          </div>
    
          <div class="form-group">
    
            <button type="submit" [disabled]="!userForm.valid" class="btn btn-success">Submit</button>
    
          </div>
    
        </form>
    
      </div>
    
      <div class="container">
    
        <ul>
    
          <li *ngFor="let user of users">{{ user.name }}</li>
    
        </ul>
    
      </div>
    
    </div>

  

If you are not familiar with the Angular2 form (annotation like ngModel or ngForm) , i suggest you to read the doc : https://angular.io/guide/forms 

Don’t worry, I will explain what is happening in the html later.

Go to your component at /src/app/app.component.ts and add : 

  

    import { Component } from '@angular/core';
    
    import { DataService } from './data.service';
    
    import { NgForm } from '@angular/forms';
    
    @Component({
    
      selector: 'app-root',
    
      templateUrl: './app.component.html',
    
      styleUrls: ['./app.component.css']
    
    })
    
    export class AppComponent {
    
      title = 'app';
    
      users: Array<any>;
    
      user: Object = {};
    
      constructor(private _dataservice: DataService) {
    
        this.getUser();
    
      }
    
      //Create a function to get the users
    
      getUser() {
    
        this._dataservice.getUsers()
    
        .subscribe((rcv) => {
    
          this.users = rcv;
    
        });
    
      }
    
      //Function that is going to be called when you submit the form
    
      postUser() {
    
       this._dataservice.postUser(this.user)
    
         .subscribe((rcv) => {
    
            console.log("Rcv : ", rcv);
    
            this.getUser(); //Call the get user after we insert one, so the user list is re-generated
    
        });
    
      }
    
    }

  

Ok, so now we have our component ( app.component.ts ) file and our html (app.component.html ) file setup. Theses two files are linked together and they have shared data. That means that if you declare a public variable in your component.ts file, you can access this variable in your html using Angular2 keywords "ngModel"  for example, or by doing {{ variableName }} . 

When we use  [(ngModel)]="user.name", that means that everything we are writing inside the** input element** is directly stored in "user.name".

The function postUser() is called by adding (ngSubmit)="postUser()" to the form.

Now you should be able to try and test, run theses command and go to http://localhost:3000/

  

    $ ng build
	$ node server

  

 

At the end, your app should be similar to this  :

![image alt text](image_2.png)




